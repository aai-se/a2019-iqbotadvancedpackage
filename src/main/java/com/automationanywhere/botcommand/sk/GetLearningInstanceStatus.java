/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 * 
 */
package com.automationanywhere.botcommand.sk;



import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.MessageFactory;
import org.json.JSONObject;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.BotCommand;
import com.automationanywhere.commandsdk.annotations.CommandPkg;

import com.automationanywhere.commandsdk.annotations.Idx;
import com.automationanywhere.commandsdk.annotations.Pkg;
import com.automationanywhere.commandsdk.annotations.Sessions;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.DataType;
import com.automationanywhere.commandsdk.annotations.Execute;


/**
 * @author Stefan Karsten
 *
 */

@BotCommand
@CommandPkg(label = "Learning Instance Status", name = "LIStatus",
        description = "Status of a Learning Instance", group_label="Instance",
        node_label = "Learning Instance Status", icon = "pkg.svg",
        return_type=DataType.DICTIONARY, return_sub_type=DataType.ANY, return_label="Details", return_required=true)
public class GetLearningInstanceStatus {
	
	  private static final Logger logger = LogManager.getLogger(GetLearningInstanceStatus.class);
	
    @Sessions
    private Map<String, Object> sessions;
    
	@Execute
    public  DictionaryValue action(@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName ,
    					      @Idx(index = "2", type = TEXT)  @Pkg(label = "LI Name" , default_value_type = STRING) @NotEmpty String li_name ) throws Exception
     {
		
		HashMap<String,Value> map = new HashMap<String,Value> ();
	    IQBotConnection connection  = (IQBotConnection) this.sessions.get(sessionName);  
	    
	    HashMap<String,String> lis = Utils.getLearningInstances(connection);
	   
	    if(lis.containsKey(li_name)) {
	    	JSONObject data  = Utils.getLearningInstanceStatus(connection, lis.get(li_name)) ;
	    	for (Iterator<String> iterator = data.keys(); iterator.hasNext();) {
				String key = iterator.next();
	    	    Object obj = data.get(key);
			    String classname = obj.getClass().getSimpleName();

			   switch (classname) {
				case "String":
				  	map.put(key, new StringValue(obj));
					break;
				case "Integer":
					map.put(key, new NumberValue(obj));
					break;
				case "Double":
					map.put(key, new NumberValue(obj));
					break;
				case "Boolean":
					map.put(key, new BooleanValue(obj));
					break;
			   }
	    	}
	    }
	    
			   
	    DictionaryValue dictmap = new DictionaryValue();
	    dictmap.set(map);
	    return dictmap;

     
     }
	
	
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
    
		
	
}
package com.automationanywhere.botcommand.sk;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;

import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


/**
 * @author Sachdeep Sivakumar
 *
 */


@BotCommand

@CommandPkg(
        name = "GetFileDetails", label = "Get File Details", group_label="Processing",
        node_label = "Get File Details",  description = "Return file details based on File Id", icon = "pkg.svg",
        return_label = "File Details", return_type = STRING, return_required = true)


public class GetDocumentDetails {
    
    
    @Sessions
    private Map<String, Object> sessions;


    @Execute
    public StringValue action(
    		@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName ,
			 @Idx(index = "2", type = TEXT)  @Pkg(label = "LI Name" , default_value_type = STRING) @NotEmpty String li_name ,
			 @Idx(index = "3", type = TEXT)  @Pkg(label = "File Id" , default_value_type = STRING) @NotEmpty String fileId 
    ) throws Exception {
    	
    	
	    IQBotConnection connection  = (IQBotConnection) this.sessions.get(sessionName);  
	    String result = "";
	    
	    HashMap<String,String> lis = Utils.getLearningInstances(connection);
	    if(lis.containsKey(li_name)) {
	    	result = Utils.downloadDocument(connection, lis.get(li_name), fileId);
	    	
	    	if (!isJSONValid(result)) {
	    		
	    		 char[] characters = result.toCharArray();
	    		 int startindex =-1;
	             int endindex =-1;
	             for (int i = 0; i < characters.length; i++) {
	            	 if (characters[i] == '=' && startindex == -1) {
	            		 startindex = i+1;
	            	 }
	            	 else if (characters[i] == '=' && startindex >= 0) {
	            		 endindex = i-1;
	            		 while (characters[endindex] != ',') {
	            			 endindex--;
	            		 }
	            		 String substring = result.substring(startindex, endindex);
	            		 String newstring = substring.replace(",", "COMMA");
	            		 result = result.replace(substring,newstring);
	            		 startindex =-1;
	            		 endindex =-1;
	    		
	            	 }
	    		
	             }
	     
	    		result = "{\"details\":"+result.replace("[{", "[{\"").replace("=", "\":\"").replace(", ","\", \"").replace("}\", \"{", "}, {\"").replace("\"File Path\":\"}", "\"File Path\":\"\"}").replace("COMMA", ",")+"}";
	    	}
	    	else {
	    		result = "{\"details\":"+result+"}";
	    	}
	    	
	    }
	    else {
	    	result="Unknown Learning Instance";
	    }
	    
	    
    	return new StringValue(result);
    }
    

    private static boolean isJSONValid(String test) {
		try {
			new JSONObject(test);
		} catch (JSONException ex) {
			try {
				new JSONArray(test);
			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}
	    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
    

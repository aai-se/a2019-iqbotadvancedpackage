package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.model.ProxyConfig;

public class IQBotConnection {

	public String url;
	public String token;
	public ProxyConfig proxy;
	
	public IQBotConnection(String url, String token, ProxyConfig proxy) {
		this.url = url;
		this.token = token;
		this.proxy = proxy;
	}
	
	public String getURL() {
		return this.url;
	}
	
	public String getToken() {
		return this.token ;
	}
	
}


package com.automationanywhere.botcommand.sk;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.bot.service.GlobalSessionContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;


/**
 * @author Sachdeep Sivakumar
 *
 */


@BotCommand

@CommandPkg(
        name = "GetFileStatus", label = "Get File Status", group_label="Processing",
        node_label = "Get File Status",  description = "Return file status based on File Id", icon = "pkg.svg",
        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "File Status", return_type = STRING, return_required = true)


public class GetDocumentStatus {
    
    
    @Sessions
    private Map<String, Object> sessions;


    @Execute
    public StringValue action(
    		@Idx(index = "1", type = TEXT)  @Pkg(label = "Session name" , default_value_type = STRING , default_value = "Default") @NotEmpty String sessionName ,
			 @Idx(index = "2", type = TEXT)  @Pkg(label = "LI Name" , default_value_type = STRING) @NotEmpty String li_name ,
			 @Idx(index = "3", type = TEXT)  @Pkg(label = "File Id" , default_value_type = STRING) @NotEmpty String fileId 
    ) throws Exception {
    	
    	
		HashMap<String,Value> map = new HashMap<String,Value> ();
	    IQBotConnection connection  = (IQBotConnection) this.sessions.get(sessionName);  
	    
	    HashMap<String,String> lis = Utils.getLearningInstances(connection);
        String fileStatus="Unknown";
	    if (lis.containsKey(li_name)) {

	    	List<String> fileList = Utils.getOutputFiles(connection, lis.get(li_name), "SUCCESS",false);
	    	if (inList(fileId, fileList)){
	    		fileStatus="Success";
	    	}
	    	else{
	    		fileList = Utils.getOutputFiles(connection, lis.get(li_name), "VALIDATION",false);
	    		if (inList(fileId, fileList)){
	    			fileStatus="Validation";
	    		}
	    		else{
	    			fileList = Utils.getOutputFiles(connection, lis.get(li_name), "UNTRAINED",false);
	    			if (inList(fileId, fileList)){
	    				fileStatus="Untrained";
	    			}
	    			else{
	    				fileList = Utils.getOutputFiles(connection, lis.get(li_name), "UNCLASSIFIED",false);
	    				if (inList(fileId, fileList)){
	    					fileStatus="Unclassified";
	    				}
		    			else{
		    				fileList = Utils.getOutputFiles(connection, lis.get(li_name), "FAIL",false);
		    				if (inList(fileId, fileList)){
		    					fileStatus="Fail";
		    				}
			    			else{
			    				fileList = Utils.getOutputFiles(connection, lis.get(li_name), "INVALID",false);
			    				if (inList(fileId, fileList)){
			    					fileStatus="Invalid";
			    				}
			    			}
		    			}

	    			}
	    		}
	    	}
	    }
	    else {
	    	  fileStatus="Unknown Learning Instance";
	    }
	    
	    
    	return new StringValue(fileStatus);
    }
    
	private boolean inList(String fileId, List<String> list) {
		boolean contains = false;
	    for (Iterator item = list.iterator(); item.hasNext();) {
			String file = (String) item.next();
			if (file.contains(fileId)) {
	        	contains = true;
	        	break;
	        }
	    }
		
		return contains;
	}


	    
    
    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
    

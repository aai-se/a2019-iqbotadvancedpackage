package com.automationanywhere.botcommand.sk;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

import org.json.JSONArray;

import com.automationanywhere.bot.model.ProxyConfig;

public class DownloadUtility {
    private static final int BUFFER_SIZE = 4096;
 
    /**
     * Downloads a file from a URL
     * @param fileURL HTTP URL of the file to be downloaded
     * @param saveDir path of the directory to save the file
     * @throws IOException
     */
    public static String downloadFile(URL url, ProxyConfig proxyConfig, String token, String filepath, String fileName,List<String> fileList) throws Exception {

		Proxy proxy = Utils.getProxy(url.toString(), proxyConfig);
        HttpURLConnection httpConn = null;
		if (proxy != null) {
			httpConn = (HttpURLConnection) url.openConnection(proxy);
		}
		else 
		{
			httpConn = (HttpURLConnection) url.openConnection();
		}
        httpConn.setRequestProperty("x-authorization", token);
        httpConn.setRequestProperty("Content-Type", "application/json");
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        OutputStream outStream = httpConn.getOutputStream();
        OutputStreamWriter outStreamWriter = new OutputStreamWriter(outStream, "UTF-8");
        outStreamWriter.write(new JSONArray(fileList).toString());
        outStreamWriter.flush();
        outStreamWriter.close();
        outStream.close();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String disposition = httpConn.getHeaderField("Content-Disposition");
            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 9,
                            disposition.length());
                }
            } 
 
            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();

            FileOutputStream outputStream = new FileOutputStream(filepath+"/"+fileName);
 
            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
 
            outputStream.close();
            inputStream.close();
 
            return fileName;
        } else {
            return "ERROR HTTP code: " + responseCode;
        }
    }
}